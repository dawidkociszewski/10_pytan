package pk.projekt.e2.game.contract;

import java.util.ArrayList;

import pk.projekt.e2.game.models.Question;

public interface IQuestionsDBManager {

	boolean zaladujDane(String fileName);	
	String pobierzLog();
	Question losujPytanie(ArrayList<Integer> uzytePytania);
	int iloscPytanWBazie();
	
}
