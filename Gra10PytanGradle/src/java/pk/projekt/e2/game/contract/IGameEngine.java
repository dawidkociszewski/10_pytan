package pk.projekt.e2.game.contract;

import javax.swing.JPanel;

public interface IGameEngine {

	public int getSzerokosc();
	public int getWysokosc();
	public JPanel getPanel();
}
