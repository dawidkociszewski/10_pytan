package pk.projekt.e2.game.app;

import java.awt.Color;

import javax.swing.JFrame;

import pk.projekt.e2.game.contract.IGameEngine;

public class GameFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int szerokosc;
	private int wysokosc;
	private String tytul;
	private IGameEngine silnikGry;

	public GameFrame(IGameEngine silnik, String tytul) {

		if (silnik == null) {
			throw new IllegalArgumentException("IGameEngine cannot be null");
		}
		silnikGry = silnik;

		szerokosc = silnikGry.getSzerokosc();
		wysokosc = silnikGry.getWysokosc();
		this.tytul = tytul;

		// Ustawienia okna
		setSize(szerokosc, wysokosc);
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle(tytul);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBackground(Color.BLACK);

		// Dodanie panelu do JFrame'a
		add(silnikGry.getPanel());
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getTytul() {
		return this.tytul;
	}

}
