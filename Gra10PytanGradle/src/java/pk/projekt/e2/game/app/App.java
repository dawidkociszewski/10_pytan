package pk.projekt.e2.game.app;

import static java.lang.System.out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pk.projekt.e2.game.contract.IPlayerHelper;
import pk.projekt.e2.game.contract.IQuestionsDBManager;
import pk.projekt.e2.game.impl.QuestionsDBManagerImpl;
import pk.projekt.e2.game.models.Question;
import pk.projekt.e2.sound.contract.IAudioManager;

public class App {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/beans.xml");

		// testAudioManager(context);

		// testQuestionsDB(context);
		
		// playerHelperTest(context);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Uzyskanie componentu gry za pomoca spring frameworka
				out.println("Getting game-frame bean...");
				try {
					JFrame game = (JFrame) context.getBean("game-frame");
					out.println("Starting the game...");
					game.setVisible(true);
				} catch (Exception ex) {
					ex.printStackTrace();
					out.println(String.format("Exception MSG: %s", ex.getMessage()));
				}
			}
		});

		// Close context after main app has been closed
		//((ClassPathXmlApplicationContext) context).close();
	}

	@SuppressWarnings("unused")
	private static void testAudioManager(ApplicationContext context) {
		IAudioManager audioM = (IAudioManager) context.getBean("audio");
		audioM.addSound("boo", "res/audio/boo.wav");
		out.println(audioM.getLatestLog());
		audioM.addSound("applause", "res/audio/applause3.wav");
		out.println(audioM.getLatestLog());
		try {
			audioM.playSound("boo");
			out.println(audioM.getLatestLog());
			audioM.playSound("applause");
			out.println(audioM.getLatestLog());
		} catch (LineUnavailableException e) {
			e.printStackTrace();
			out.println("LineUnavailableException");
		} catch (IOException e) {
			e.printStackTrace();
			out.println("IOException");
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
			out.println("Unsupported audio file");
		} catch (InterruptedException e) {
			e.printStackTrace();
			out.println("InterruptedException");
		}
	}

	@SuppressWarnings("unused")
	private static void testQuestionsDB(ApplicationContext context) {
		String path = "res/data.xml";

		IQuestionsDBManager qdbm = (IQuestionsDBManager) context.getBean("qdbm");

		out.println("Test");
		out.println("------------------------------------------");
		out.println("Metoda: zaladujDane");
		out.println("------------------------------------------");

		qdbm.zaladujDane(path);
		out.println(qdbm.pobierzLog());

		out.println("Test");
		out.println("------------------------------------------");
		out.println("Metoda: losujPytanie");
		out.println("------------------------------------------");

		ArrayList<Integer> uzyte = new ArrayList<>();
		uzyte.add(2);
		Question pytanie = qdbm.losujPytanie(uzyte);
		out.println(qdbm.pobierzLog());
		out.println("[" + pytanie.getGoodAns() + "] Wylosowano: " + pytanie.getQuestion());
		out.println("(1) " + pytanie.getAns1());
		out.println("(2) " + pytanie.getAns2());
		out.println("(3) " + pytanie.getAns3());
		out.println("(4) " + pytanie.getAns4());

		out.println("------------------------------------------");
		out.println("***************KONIEC TESTU***************");
		out.println("------------------------------------------");

		// out.println(qdbm.pobierzLog());
		((QuestionsDBManagerImpl) qdbm).displayAllData();
	}
	
	@SuppressWarnings("unused")
	private static void playerHelperTest(ApplicationContext context) {
		IPlayerHelper helper = (IPlayerHelper) context.getBean("player-helper");
		Random r = new Random();
		
		int dobraOdp = r.nextInt(4) + 1;
		
		Integer[] zleOdp = helper.fiftyFifty(dobraOdp);
		
		out.println("------------------------------------------");
		out.println("***        PLAYER HELPER TEST          ***");
		out.println("------------------------------------------");
		
		out.println(String.format("Dobra odpowiedz to: %d", dobraOdp));
		out.println(String.format("Zle odpowiedzi do wywalenia to: %d, %d", zleOdp[0], zleOdp[1]));
		
		out.println("------------------------------------------");
		out.println("***************KONIEC TESTU***************");
		out.println("------------------------------------------");
	}

}
