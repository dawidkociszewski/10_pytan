package pk.projekt.e2.game.app;

import javax.swing.ImageIcon;

public class Guzik {

	private String nazwa;
	private int x;
	private int y;

	private ImageIcon obraz;
	private ImageIcon obrazNajechany;
	private ImageIcon aktualnyObraz;
	
	private boolean widoczny;

	public Guzik(String nazwa, int x, int y, ImageIcon obraz, ImageIcon najechany) {
		this.nazwa = nazwa;
		this.x = x;
		this.y = y;
		this.obraz = obraz;
		this.obrazNajechany = najechany;
		this.widoczny = true;
	}

	public String getNazwa() {
		return nazwa;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public ImageIcon getObraz() {
		return this.obraz;
	} 
	
	public ImageIcon getObrazNajechany() {
		return this.obrazNajechany;
	}
	
	public ImageIcon getAktualnyObraz() {
		return this.aktualnyObraz;
	}
	
	public void setAktualnyObraz(ImageIcon img) {
		this.aktualnyObraz = img;
	}
	
	public boolean getWidoczny() {
		return this.widoczny;
	}
	
	public void setWidoczny(boolean widoczny) {
		this.widoczny = widoczny;
	}
 
	public boolean jestKolizja(int kolizjaX, int kolizjaY) {
		int szerokosc = obraz.getImage().getWidth(null);
		int wysokosc = obraz.getImage().getHeight(null);
		
		if (kolizjaX > this.x && kolizjaX < this.x + szerokosc && kolizjaY > this.y && kolizjaY < this.y + wysokosc) {
			return true;
		}

		return false;
	}
}
