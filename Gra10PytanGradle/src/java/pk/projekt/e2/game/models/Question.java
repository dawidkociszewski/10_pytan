package pk.projekt.e2.game.models;

import java.io.Serializable;

public class Question implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String question;
	private String ans1;
	private String ans2;
	private String ans3;
	private String ans4;
	private int goodAns;

	public Question(String q, String a1, String a2, String a3, String a4, int goodAns) {
		this.question = q;
		this.ans1 = a1;
		this.ans2 = a2;
		this.ans3 = a3;
		this.ans4 = a4;

		this.goodAns = goodAns;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGoodAns() {
		return goodAns;
	}

	public void setGoodAns(int goodAns) {
		this.goodAns = goodAns;
	}

	public String getQuestion() {
		return question;
	}

	public String getAns1() {
		return ans1;
	}

	public String getAns2() {
		return ans2;
	}

	public String getAns3() {
		return ans3;
	}

	public String getAns4() {
		return ans4;
	}

	public void removeAnswer(int numer) {

		switch (numer) {
		case 1: {
			ans1 = "";
			break;
		}
		case 2: {
			ans2 = "";
			break;
		}
		case 3: {
			ans3 = "";
			break;
		}
		case 4: {
			ans4 = "";
			break;
		}
		default:
			break;
		}
	}
}
