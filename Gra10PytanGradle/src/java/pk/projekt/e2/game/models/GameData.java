package pk.projekt.e2.game.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GameData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Jesli koniec gry to pokaz wynik
	private boolean koniecGry;

	// *** Zmienne gry ***
	private String nick;
	private int iloscPodpowiedzi;
	private int wynik;
	private int numerAktualnegoPytania;
	private Question aktualnePytanie;
	private ArrayList<Integer> uzytePytania;

	public boolean isKoniecGry() {
		return koniecGry;
	}

	public void setKoniecGry(boolean koniecGry) {
		this.koniecGry = koniecGry;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getIloscPodpowiedzi() {
		return iloscPodpowiedzi;
	}

	public void setIloscPodpowiedzi(int iloscPodpowiedzi) {
		this.iloscPodpowiedzi = iloscPodpowiedzi;
	}

	public int getWynik() {
		return wynik;
	}

	public void setWynik(int wynik) {
		this.wynik = wynik;
	}

	public int getNumerAktualnegoPytania() {
		return numerAktualnegoPytania;
	}

	public void setNumerAktualnegoPytania(int numerAktualnegoPytania) {
		this.numerAktualnegoPytania = numerAktualnegoPytania;
	}

	public Question getAktualnePytanie() {
		return aktualnePytanie;
	}

	public void setAktualnePytanie(Question aktualnePytanie) {
		this.aktualnePytanie = aktualnePytanie;
	}
	
	public ArrayList<Integer> getUzytePytania() {
		return this.uzytePytania;
	}
	
	public void setUzytePytania(ArrayList<Integer> uzyte) {
		this.uzytePytania = uzyte;
	}

}
