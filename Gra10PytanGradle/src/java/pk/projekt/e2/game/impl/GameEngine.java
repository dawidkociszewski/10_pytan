package pk.projekt.e2.game.impl;

import static java.lang.System.out;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pk.projekt.e2.game.app.Guzik;
import pk.projekt.e2.game.contract.IGameEngine;
import pk.projekt.e2.game.contract.IPlayerHelper;
import pk.projekt.e2.game.contract.IQuestionsDBManager;
import pk.projekt.e2.game.models.GameData;
import pk.projekt.e2.sound.contract.IAudioManager;
import pl.e3.components.IRanking;

public class GameEngine extends JPanel implements IGameEngine, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// OKNO
	private int szerokosc;
	private int wysokosc;
	private ImageIcon tlo;
	private ImageIcon tloGry;

	// *** Czcionki *** PL-> PLAIN, B-> BOLD
	private Font chinskaCzcionkaPL;
	private Font chinskaCzcionkaB;
	private Font chinskaCzcionkaSredniaPL;
	//private Font chinskaCzcionkaSredniaB;
	private Font chinskaCzcionkaDuza;
	private Font zwyklaCzcionka;

	// Inne komponenty
	private IQuestionsDBManager qdbm;
	private IPlayerHelper helper;
	private IAudioManager audio;
	private IRanking ranking;

	// *** Zmienne warunkowe ***
	// Je�li gra nie trwa to jestesmy w menu glownym
	private boolean trwaGra;
	private boolean dzwiekiWlaczone;
	private boolean pokazRanking;
	private boolean pytaniaZaladowanePoprawnie;

	// *** Zmienne gry ***
	private GameData gd;
	private int maxDlugoscPytania = 60;
	private int nickMaxLength = 25;
	private String pyt;
	private String pyt1;
	private String pyt2;
	private String koniecGryTytul;
	private String koniecGryWiadomosc1;
	private String koniecGryWiadomosc2;
	private String koniecGryWiadomosc3;
	private String koniecGryWiadomosc4;
	private JFrame messageBox;
	private List<Map.Entry<?, Integer>> rankingTop10;
	private int index;

	// Guziki
	// *** MENU GLOWNE ***
	private Guzik nowaGraBTN;
	private Guzik wczytajBTN;
	private Guzik rankingBTN;
	private Guzik opcjeBTN;
	private Guzik pomocBTN;
	private Guzik wyjscieBTN;
	// *** GRA ***
	private Guzik aBTN;
	private Guzik bBTN;
	private Guzik cBTN;
	private Guzik dBTN;

	// Myszka
	private Myszka myszka;
	// Te zmienne informuja od pozycji klikniecia myszki
	private int mx, my;
	// Te zmienne informuja o ruchu myszki
	private int motionX, motionY;

	// Animatory
	private transient Thread animatorGuzika;
	private final int opoznienieGuzika = 60;

	public GameEngine(IQuestionsDBManager iqdbm, IPlayerHelper helper, IAudioManager audio, IRanking ranking) {
		// Sprawdzanie komponentow
		if (iqdbm == null) {
			throw new IllegalArgumentException("IQuestionsDBManager cannot be null");
		} else {
			this.qdbm = iqdbm;
		}
		if (helper == null) {
			throw new IllegalArgumentException("IPlayerHelper cannot be null");
		} else {
			this.helper = helper;
		}
		if (audio == null) {
			throw new IllegalArgumentException("IAudioManager cannot be null");
		} else {
			this.audio = audio;
		}
		if (ranking == null) {
			throw new IllegalArgumentException("IRanking cannot be null");
		} else {
			this.ranking = ranking;
		}

		// Inicjalizacja okna
		szerokosc = 1024;
		wysokosc = 720;
		setBackground(Color.BLACK);
		setDoubleBuffered(true);
		chinskaCzcionkaPL = pobierzCzcionke(Font.PLAIN, 20);
		chinskaCzcionkaB = pobierzCzcionke(Font.BOLD, 20);
		chinskaCzcionkaSredniaPL = pobierzCzcionke(Font.PLAIN, 30);
		//chinskaCzcionkaSredniaB = pobierzCzcionke(Font.BOLD, 30);
		chinskaCzcionkaDuza = pobierzCzcionke(Font.BOLD, 50);
		zwyklaCzcionka = new Font("Monospaced", Font.PLAIN, 20);

		// Inicjalizacja zmiennych warunkowych
		trwaGra = false;
		dzwiekiWlaczone = true;
		pokazRanking = false;

		// Myszka
		myszka = new Myszka();
		addMouseListener(myszka);
		addMouseMotionListener(myszka);

		// Animatory

		// Inicjalizacja obrazkow
		// Obrazki tla
		tlo = new ImageIcon(getClass().getClassLoader().getResource("obrazki/mainMenuBG1024.png"));
		tloGry = new ImageIcon(getClass().getClassLoader().getResource("obrazki/gameBG1024.png"));

		initGuzikow();

		initKomponentow();

		gd = new GameData();
		messageBox = new JFrame();
	}
	
	private JFrame getMessageBox() {
		if(messageBox == null) {
			messageBox = new JFrame();
		}
		
		return messageBox;
	}

	private void initGuzikow() {
		// Obrazki guzikow
		// *** OBRAZKI MENU GLOWNEGO ***
		ImageIcon nowaImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/nowagra.png"));
		ImageIcon nowaImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/nowagraHover.png"));
		ImageIcon wczytajImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/wczytajgre.png"));
		ImageIcon wczytajImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/wczytajgreHover.png"));
		ImageIcon rankingImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/ranking.png"));
		ImageIcon rankingImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/rankingHover.png"));
		ImageIcon opcjeImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/opcje.png"));
		ImageIcon opcjeImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/opcjeHover.png"));
		ImageIcon pomocImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/pomoc.png"));
		ImageIcon pomocImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/pomocHover.png"));
		ImageIcon wyjscieImg = new ImageIcon(getClass().getClassLoader().getResource("obrazki/wyjscie.png"));
		ImageIcon wyjscieImg2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/wyjscieHover.png"));
		// *** OBRAZKI GRY ***
		ImageIcon odpA = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpa.png"));
		ImageIcon odpA2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpaHover.png"));
		ImageIcon odpB = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpb.png"));
		ImageIcon odpB2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpbHover.png"));
		ImageIcon odpC = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpc.png"));
		ImageIcon odpC2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpcHover.png"));
		ImageIcon odpD = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpd.png"));
		ImageIcon odpD2 = new ImageIcon(getClass().getClassLoader().getResource("obrazki/odpdHover.png"));

		// Inicjalizacja guzikow
		// *** GUZIKI MENU GLOWNEGO ***
		nowaGraBTN = new Guzik("nowa_gra", 90, 510, nowaImg, nowaImg2);
		nowaGraBTN.setAktualnyObraz(nowaImg);

		wczytajBTN = new Guzik("wczytaj_gre", 310, 510, wczytajImg, wczytajImg2);
		wczytajBTN.setAktualnyObraz(wczytajImg);

		rankingBTN = new Guzik("ranking", 530, 510, rankingImg, rankingImg2);
		rankingBTN.setAktualnyObraz(rankingImg);

		opcjeBTN = new Guzik("opcje", 750, 510, opcjeImg, opcjeImg2);
		opcjeBTN.setAktualnyObraz(opcjeImg);

		pomocBTN = new Guzik("pomoc", 90, 575, pomocImg, pomocImg2);
		pomocBTN.setAktualnyObraz(pomocImg);

		wyjscieBTN = new Guzik("wyjscie", 835, 575, wyjscieImg, wyjscieImg2);
		wyjscieBTN.setAktualnyObraz(wyjscieImg);

		// *** GUZIKI GRY ***
		aBTN = new Guzik("a", 88, 510, odpA, odpA2);
		aBTN.setAktualnyObraz(odpA);

		bBTN = new Guzik("b", 308, 510, odpB, odpB2);
		bBTN.setAktualnyObraz(odpB);

		cBTN = new Guzik("c", 528, 510, odpC, odpC2);
		cBTN.setAktualnyObraz(odpC);

		dBTN = new Guzik("d", 748, 510, odpD, odpD2);
		dBTN.setAktualnyObraz(odpD);
	}

	private void initKomponentow() {
		// *** IQuestionsDBManager ***
		
		pytaniaZaladowanePoprawnie = qdbm.zaladujDane("res/data.xml");
		out.println(qdbm.pobierzLog());
		
		// *** IAudioManager ***
		audio.addSound("boo", "res/audio/boo.wav");
		out.println(String.format("[IAudioManager]%s", audio.getLatestLog()));
		audio.addSound("applause", "res/audio/applause3.wav");
		out.println(String.format("[IAudioManager]%s", audio.getLatestLog()));
	}

	public void addNotify() {
		super.addNotify();

		// WATEK ANIMATORA GUZIKOW
		animatorGuzika = new Thread(new AnimatorGuzikow(opoznienieGuzika));
		animatorGuzika.start();
	}

	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		// Ustawienie czionki
		resetujCzcionke(g2d);

		// Sprawdz czy rysowac interfejs gry czy menu glownego
		if (trwaGra) {
			// RYSOWANIE TLA GRY
			g2d.drawImage(tloGry.getImage(), 0, 0, null);

			if (gd.isKoniecGry()) {
				// *** Rysuj wynik koncowy gry ***
				g2d.setColor(Color.GREEN);
				g2d.setFont(chinskaCzcionkaDuza);
				g2d.drawString(koniecGryTytul, 120, 175);

				// *** Rysuj wiadomosc koncowa 1 ***
				g2d.setColor(Color.WHITE);
				g2d.setFont(chinskaCzcionkaSredniaPL);
				g2d.drawString(koniecGryWiadomosc1, 120, 245);
				// *** Rysuj wiadomosc koncowa 2 ***
				g2d.drawString(koniecGryWiadomosc2, 120, 295);
				// *** Rysuj wiadomosc koncowa 3 ***
				g2d.drawString(koniecGryWiadomosc3, 120, 345);
				// *** Rysuj wiadomosc koncowa 4 ***
				g2d.drawString(koniecGryWiadomosc4, 120, 445);

			} else {
				// *** Rysuj intergejs gry ***

				// *** Nick gracza ***
				g2d.setColor(Color.YELLOW);
				g2d.setFont(chinskaCzcionkaB);
				g2d.drawString(String.format("Gracz: %s", gd.getNick()), 90, 95);

				// *** Rysowanie pytania ***
				g2d.setColor(Color.GREEN);
				g2d.setFont(chinskaCzcionkaSredniaPL);
				pyt = gd.getAktualnePytanie().getQuestion();
				// pyt = "SUPER DLUGASNE PYTANIE BLABLABLAL LSD LALD ALSD LAS LASLASLAS SUPER DLUGASNE PYTANIE BLABLABLAL LSD LALD ALSD LAS LAS LAS LAS";
				if (pyt.length() > maxDlugoscPytania) {
					// Zawijanie tekstu jesli jest dluzszy niz maksymalna
					// dlugosc
					pyt1 = pyt.substring(0, maxDlugoscPytania + 1);
					if (pyt.length() > 2 * maxDlugoscPytania) {
						pyt2 = pyt.substring(maxDlugoscPytania + 1, 2 * maxDlugoscPytania);
						pyt2 = pyt2.concat("...");
					} else {
						pyt2 = pyt.substring(maxDlugoscPytania + 1, pyt.length());
					}

					g2d.drawString(String.format("%d. %s", gd.getNumerAktualnegoPytania(), pyt1), 80, 135);
					g2d.drawString(pyt2, 110, 170);

				} else {
					g2d.drawString(String.format("%d. %s", gd.getNumerAktualnegoPytania(), pyt), 80, 135);
				}

				g2d.setColor(Color.WHITE);
				// *** Odpowiedz A ***
				g2d.drawString(String.format("A. %s", gd.getAktualnePytanie().getAns1()), 100, 220);
				// *** Odpowiedz B ***
				g2d.drawString(String.format("B. %s", gd.getAktualnePytanie().getAns2()), 100, 290);
				// *** Odpowiedz C ***
				g2d.drawString(String.format("C. %s", gd.getAktualnePytanie().getAns3()), 100, 360);
				// *** Odpowiedz D ***
				g2d.drawString(String.format("D. %s", gd.getAktualnePytanie().getAns4()), 100, 430);

				resetujCzcionke(g2d);

				// *** Rysowanie guzikow gry ***
				// *** A ***
				if (aBTN.getWidoczny()) {
					g2d.drawImage(aBTN.getAktualnyObraz().getImage(), aBTN.getX(), aBTN.getY(), null);
				}
				// *** B ***
				if (bBTN.getWidoczny()) {
					g2d.drawImage(bBTN.getAktualnyObraz().getImage(), bBTN.getX(), bBTN.getY(), null);
				}
				// *** C ***
				if (cBTN.getWidoczny()) {
					g2d.drawImage(cBTN.getAktualnyObraz().getImage(), cBTN.getX(), cBTN.getY(), null);
				}
				// *** D ***
				if (dBTN.getWidoczny()) {
					g2d.drawImage(dBTN.getAktualnyObraz().getImage(), dBTN.getX(), dBTN.getY(), null);
				}

				// *** Guzik 'POMOC' w trakcie gry ***
				g2d.drawImage(pomocBTN.getAktualnyObraz().getImage(), pomocBTN.getX(), pomocBTN.getY(), null);
				// *** Ilosc podpowiedzi ***
				g2d.setColor(Color.WHITE);
				g2d.setFont(chinskaCzcionkaPL);
				g2d.drawString(String.format("(Pozostalo podpowiedzi: %s)", gd.getIloscPodpowiedzi()), 210, 597);
			}

			// *** INTERFEJS GRY NIEZALEZNY OD ZMIENNEJ 'koniecGry'
			// *** Rysowanie punktow ***
			g2d.setColor(Color.WHITE);
			g2d.setFont(chinskaCzcionkaPL);
			g2d.drawString("ZDOBYTE PUNKTY: ", 680, 95);
			g2d.setColor(Color.RED);
			g2d.setFont(chinskaCzcionkaDuza);
			g2d.drawString(String.format("%d PKT", gd.getWynik()), 825, 95);

			// *** Rysowanie stanu punktow? ***
			g2d.setFont(chinskaCzcionkaSredniaPL);
			// *** 10 PKT ***
			ustawKolorPunktu(g2d, 10);
			g2d.drawString("10 PKT", 830, 210);
			// *** 9 PKT ***
			ustawKolorPunktu(g2d, 9);
			g2d.drawString("9 PKT", 830, 240);
			// *** 8 PKT ***
			ustawKolorPunktu(g2d, 8);
			g2d.drawString("8 PKT", 830, 270);
			// *** 7 PKT ***
			ustawKolorPunktu(g2d, 7);
			g2d.drawString("7 PKT", 830, 300);
			// *** 6 PKT ***
			ustawKolorPunktu(g2d, 6);
			g2d.drawString("6 PKT", 830, 330);
			// *** 5 PKT ***
			ustawKolorPunktu(g2d, 5);
			g2d.drawString("5 PKT", 830, 360);
			// *** 4 PKT ***
			ustawKolorPunktu(g2d, 4);
			g2d.drawString("4 PKT", 830, 390);
			// *** 3 PKT ***
			ustawKolorPunktu(g2d, 3);
			g2d.drawString("3 PKT", 830, 420);
			// *** 2 PKT ***
			ustawKolorPunktu(g2d, 2);
			g2d.drawString("2 PKT", 830, 450);
			// *** 1 PKT ***
			ustawKolorPunktu(g2d, 1);
			g2d.drawString("1 PKT", 830, 480);

			// Reset czcionki na "zwykla"
			resetujCzcionke(g2d);

		} else {
			// *** Gra nie trwa, wiec rysuj interfejs Menu Glownego ***			
			
			if(pokazRanking) {
				// *** Rysowanie rankingu ***
				// RYSOWANIE TLA
				g2d.drawImage(tloGry.getImage(), 0, 0, null);
				
				// Rysuj ranking
				g2d.setColor(Color.GREEN);
				g2d.setFont(chinskaCzcionkaDuza);
				g2d.drawString("Ranking", 120, 125);

				if(rankingTop10 == null) {					
					// *** Rysuj wiadomosc o braku wpisow ***
					g2d.setColor(Color.WHITE);
					g2d.setFont(chinskaCzcionkaSredniaPL);
					
					g2d.drawString("Brak wpis�w w rankingu", 120, 190);				
					
				} else {
					
					if (rankingTop10.isEmpty()) {
						// *** Rysuj wiadomosc o braku wpisow ***
						g2d.setColor(Color.WHITE);
						g2d.setFont(chinskaCzcionkaSredniaPL);

						g2d.drawString("Brak wpis�w w rankingu", 120, 190);
						
					} else {
						
						// Ranking nie jest pusty. Rysuj ranking
						g2d.setColor(Color.WHITE);
						g2d.setFont(chinskaCzcionkaSredniaPL);
						
						int h = 150;
						index = 1;
				        for (java.util.Map.Entry<?, Integer> entry : rankingTop10) {
				        	h += 40;
				        	g2d.drawString(String.format("%d. %d pkt -> %s", index, entry.getValue(), entry.getKey()), 120, h);
				        	index++;
						}
					}
					
				}
				
				resetujCzcionke(g2d);
				
			} else {
				// RYSOWANIE TLA
				g2d.drawImage(tlo.getImage(), 0, 0, null);
				
				// *** Rysuj Menu Glowne ***
				// *** Rysowanie guzikow menu glownego ***
				g2d.drawImage(nowaGraBTN.getAktualnyObraz().getImage(), nowaGraBTN.getX(), nowaGraBTN.getY(), null);
				g2d.drawImage(wczytajBTN.getAktualnyObraz().getImage(), wczytajBTN.getX(), wczytajBTN.getY(), null);
				g2d.drawImage(rankingBTN.getAktualnyObraz().getImage(), rankingBTN.getX(), rankingBTN.getY(), null);
				g2d.drawImage(opcjeBTN.getAktualnyObraz().getImage(), opcjeBTN.getX(), opcjeBTN.getY(), null);

				// *** Guzik 'POMOC' w menu glownym ***
				g2d.drawImage(pomocBTN.getAktualnyObraz().getImage(), pomocBTN.getX(), pomocBTN.getY(), null);
			}			
		}

		// Guziki niezalezne od stanu gry
		g2d.drawImage(wyjscieBTN.getAktualnyObraz().getImage(), wyjscieBTN.getX(), wyjscieBTN.getY(), null);

		// TEST MYSZKI
		//g2d.setColor(Color.GRAY);
		//g2d.drawString(String.format("(MX, MY)=(%d, %d)", mx, my), 10, 30);
		//g2d.drawString(String.format("Motion=(%d, %d)", motionX, motionY), 10, 50);

		// Odswiezanie grafiki
		// odswiezGrafike();
		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	private void odswiezGrafike() {
		this.repaint();
	}

	@Override
	public int getSzerokosc() {
		return this.szerokosc;
	}

	@Override
	public int getWysokosc() {
		return this.wysokosc;
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	// *** PRYWATNE METODY ***

	private void rozpocznijGre() {
		// Wlaczanie na pale widocznosc guzikow
		aBTN.setWidoczny(true);
		bBTN.setWidoczny(true);
		cBTN.setWidoczny(true);
		dBTN.setWidoczny(true);
		trwaGra = true;

		gd.setKoniecGry(false);
		gd.setIloscPodpowiedzi(1);
		gd.setWynik(0);
		gd.setNumerAktualnegoPytania(1);
		gd.setUzytePytania(new ArrayList<Integer>());
		gd.setAktualnePytanie(qdbm.losujPytanie(gd.getUzytePytania()));

		// For DEBUG purpose
		wypiszUzyte();
	}
	
	private boolean istniejaZapisaneGry() {
		Object[] options = {"Rozpocznij nowa gre", "Anuluj"};
		
		File f = new File("save.ser");
        if (!f.exists()) {
            return false;
        }
		
		int wybor = JOptionPane.showOptionDialog(getMessageBox(),
			"Istnieja zapisana gra. Czy mimo to chcesz rozpoczac nowa gre?",
			"Potwierdzenie rozpoczecia nowej gry",
			JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,
			options,
			options[0]);
		
		switch (wybor) {
		case 0: {
			// Rozpoczyna nowa gre
			return false;
		}
		case 1: {
			// Nie rozpoczyna nowej gry
			return true;
		}
		default:
			return false;
		}
	}
	
	private boolean zapytajONick() {
		String nick;	
		
		// Pytaj 
		do {
			nick = nickDialog();
			
			// Uzytkownik nacisnal 'CANCEL' lub ESC
			if(nick == null) {
				return false;
			}
			
		} while (nick.length() > 25 || nick.isEmpty());
		
		gd.setNick(nick);
		
		return true;
	}
	
	private String nickDialog() {
		return JOptionPane.showInputDialog(
		        getMessageBox(), 
		        String.format("Podaj nick: (max %d znakow) i niepusty", nickMaxLength),
		        "Nowa Gra", 
		        JOptionPane.QUESTION_MESSAGE
		    );
	}
	
	private void pokazWiadomosc(String tytul, String wiadomosc) {
		JOptionPane.showMessageDialog(getMessageBox(), wiadomosc, tytul, JOptionPane.INFORMATION_MESSAGE);
	}
	
	private String zbudujWiadomoscPowitalna() {
		StringBuilder sb = new StringBuilder();
        sb.append("Witaj ").append(gd.getNick()).append(System.getProperty("line.separator"));
        sb.append("Za chwile rozpoczniesz turniej 10 Pytan!").append(System.getProperty("line.separator"));
        if(dzwiekiWlaczone) {
        	sb.append("Obecnie dzwieki sa wlaczone, dlatego prosze wlacz glosniki.").append(System.getProperty("line.separator"));
        	sb.append("Jesli nie masz glosnikow to prosze wylacz dzwiek w OPCJE w menu glownym.").append(System.getProperty("line.separator"));
        	sb.append("Dzieki temu nie bedzie przerwy na zagranie dzwieku po kazdej odpowiedzi.").append(System.getProperty("line.separator"));
        }
        sb.append(System.getProperty("line.separator")).append("Powodzenia!");
        
        return sb.toString();
	}
	
	private String zbudujInstrukcjeGry() {
		StringBuilder sb = new StringBuilder();
        sb.append("Witaj w Teleturnieju 10 PYTAN!").append(System.getProperty("line.separator"));
        sb.append("Gra polega na odpowiedzeniu na serie 10 pytan.").append(System.getProperty("line.separator"));
        sb.append("Kazde pytanie ma TYLKO 1 poprawna odpowiedz.").append(System.getProperty("line.separator"));
        sb.append("W razie problemow mozna skorzystac z kola ratunkowego.").append(System.getProperty("line.separator"));
        sb.append("Jednak z takiego kola ratunkowego mozna skorzystac TYLKO RAZ!.").append(System.getProperty("line.separator"));        
        
        sb.append(System.getProperty("line.separator")).append("Autorzy (Zesp� projektowy 21):").append(System.getProperty("line.separator"));
        sb.append("-> Dawid Kociszewski").append(System.getProperty("line.separator"));
        sb.append("-> Kamil Jaros").append(System.getProperty("line.separator"));
        sb.append("-> Piotr Niczyporuk").append(System.getProperty("line.separator"));;
        
        sb.append(System.getProperty("line.separator")).append("Powodzenia!");
        
        return sb.toString();
	}

	private void sprawdzPytanie(int numer) {
		// Jesli odpowiedz jest prawidlowa
		if (gd.getAktualnePytanie().getGoodAns() == numer) {
			
			// Dzwiek po prawidlowej odpowiedzi
			if(dzwiekiWlaczone) {
				try {
					audio.playSound("applause");
				} catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException e) {					
					e.printStackTrace();
					out.println("Blad podczas grania dzwieku res/audio/applause3.wav");
				}
			}			
			
			// Jesli to nie byla ostatnie pytanie
			if (gd.getNumerAktualnegoPytania() < 10) {
				nastepnePytanie();
			} else if (gd.getNumerAktualnegoPytania() == 10) {
				// Zakoncz gre oglaszajac zwyciestwo (tylko jesli odpowiedziano poprawnie na wszystkie 10 pytan)
				zakonczGre(true);
			}
		} else {
			// Dzwiek po blednej odpowiedzi
			if(dzwiekiWlaczone) {
				try {
					audio.playSound("boo");
				} catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException e) {					
					e.printStackTrace();
					out.println("Blad podczas grania dzwieku res/audio/boo.wav");
				}
			}			
			
			// Zakoncz gre bez oglaszania zwyciestwa (nie odpowiedziano na wszystkie 10 pytan)
			zakonczGre(false);
		}
	}

	private void nastepnePytanie() {
		// Wlaczanie na pale widocznosc guzikow
		aBTN.setWidoczny(true);
		bBTN.setWidoczny(true);
		cBTN.setWidoczny(true);
		dBTN.setWidoczny(true);

		// Zwieksz wynik
		gd.setWynik(gd.getWynik() + 1);
		// Zwieksz licznik pytania
		gd.setNumerAktualnegoPytania(gd.getNumerAktualnegoPytania() + 1);
		// Wylosuj nowe pytanie
		gd.setAktualnePytanie(qdbm.losujPytanie(gd.getUzytePytania()));

		// For DEBUG purpose
		wypiszUzyte();
	}

	private void zakonczGre(boolean zwyciestwo) {
		gd.setKoniecGry(true);
		if (zwyciestwo) {
			koniecGryTytul = "GRATULACJE! WYGRALES!";
			gd.setWynik(10);
			koniecGryWiadomosc1 = "Odpowiedziale�/a� na wszystkie pytania!";
			koniecGryWiadomosc2 = String.format("Tw�j wynik to: %d punkt�w", gd.getWynik());
			koniecGryWiadomosc3 = String.format("Dzi�kujemy Ci %s za gr�", gd.getNick());
			koniecGryWiadomosc4 = "Zapraszamy ponownie!";

		} else {
			koniecGryTytul = "Koniec Gry";
			String pytanStr = "pytanie";
			int iloscOdpowiedzianych = gd.getNumerAktualnegoPytania() - 1;
			if (iloscOdpowiedzianych >= 2 && iloscOdpowiedzianych <= 4) {
				pytanStr = "pytania";
			} else if (iloscOdpowiedzianych > 4 || iloscOdpowiedzianych == 0) {
				pytanStr = "pyta�";
			}
			koniecGryWiadomosc1 = String.format("Odpowiedziale�/a� na %s %s", iloscOdpowiedzianych, pytanStr);
			koniecGryWiadomosc2 = String.format("Tw�j wynik to: %d punkt�w", gd.getWynik());
			koniecGryWiadomosc3 = String.format("Dzi�kujemy Ci %s za gr�", gd.getNick());
			koniecGryWiadomosc4 = "Nast�pnym razem p�jdzie Ci lepiej!";
		}
		
		ranking.Add(gd.getNick(), gd.getWynik());
	}
	
	private boolean przerwijGre() {
		Object[] options = {"Przerwij gre", "Graj dalej"};
		
		int wybor = JOptionPane.showOptionDialog(getMessageBox(),
			"Czy chcesz przerwac biezaca gre?",
			"Przerywanie gry",
			JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,
			options,
			options[1]);
		
		switch (wybor) {
		case 0: {
			// Wybrano 'Przerwij gre'
			return true;
		}
		case 1: {
			// Wybrano 'Graj dalej'
			return false;
		}
		default:
			return false;
		}
	}

	private void wypiszUzyte() {
		if (!gd.getUzytePytania().isEmpty()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Uzyte pytania: ");
			for (int i = 0; i < gd.getUzytePytania().size(); i++) {
				sb.append(String.format("%d, ", gd.getUzytePytania().get(i)));
			}

			out.println(sb.toString());
		} else {
			out.println("Lista uzytych pytan jest pusta...");
		}
	}

	private void uzyjKola() {
		Integer[] zlePytania = helper.fiftyFifty(gd.getAktualnePytanie().getGoodAns());

		// Usuniecie pierwszej zlej
		switch (zlePytania[0]) {
		case 1: {
			aBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(1);

			break;
		}
		case 2: {
			bBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(2);

			break;
		}
		case 3: {
			cBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(3);

			break;
		}
		case 4: {
			dBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(4);

			break;
		}
		default:
			break;
		}

		// Usuniecie drugiej zlej odpowiedzi
		switch (zlePytania[1]) {
		case 1: {
			aBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(1);

			break;
		}
		case 2: {
			bBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(2);

			break;
		}
		case 3: {
			cBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(3);

			break;
		}
		case 4: {
			dBTN.setWidoczny(false);
			gd.getAktualnePytanie().removeAnswer(4);

			break;
		}
		default:
			break;
		}

		gd.setIloscPodpowiedzi(0);
	}
	
	private void pokazOpcje() {		
		Object[] options = {"Wlacz Dzwiek", "Wylacz dzwiek"};
		
		int wybor = JOptionPane.showOptionDialog(getMessageBox(),
			"Opcje dzwieku:",
			"Opcje",
			JOptionPane.YES_NO_CANCEL_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null,
			options,
			options[0]);
		
		switch (wybor) {
			case 0: {
				// Wybrano 'Wlacz Dzwiek'
				dzwiekiWlaczone = true;
				out.println("[OPCJE] Wlaczono dzwiek");
				break;
			}
			case 1: {
				// Wybrano 'Wylacz dzwiek'
				dzwiekiWlaczone = false;
				out.println("[OPCJE] Wylaczono dzwiek");
				break;
			}
			default: {
				// Domylsna wartosc -> Wlaczone dzwieki
				dzwiekiWlaczone = true;
				out.println("[OPCJE] Wlaczono dzwiek");
				break;
			}				
		}
	}

	private Font pobierzCzcionke(int style, float wielkosc) {
		try {
			out.println("Pobieranie czcinki chineserocksrg");
			// Returned font is of pt size 1
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File("res/chineserocksrg.ttf"));

			// Derive and return a 12 pt version:
			// Need to use float otherwise
			// it would be interpreted as style

			out.println("Pobranie czcionki 'chineserocksrg' zakonczone");
			return font.deriveFont(style, wielkosc);

		} catch (IOException | FontFormatException e) {
			// Zwroc jakas czcionke jak nie mozna zaladowac wlasnej
			out.println("Blad podczas pobierania czcionki 'chineserocksrg'. Zwrocono 'Monospaced'");
			return new Font("Monospaced", Font.PLAIN, 20);
		}
	}

	private void resetujCzcionke(Graphics g) {
		g.setFont(zwyklaCzcionka);
		g.setColor(Color.WHITE);
	}

	private void ustawKolorPunktu(Graphics g, int pkt) {
		if (gd.getNumerAktualnegoPytania() > pkt) {
			g.setColor(Color.WHITE);
		} else if (gd.getNumerAktualnegoPytania() == pkt) {
			g.setColor(Color.GREEN);
		} else if (gd.getNumerAktualnegoPytania() < pkt) {
			g.setColor(Color.RED);
		}
	}
	
	// *** SERIALIZACJA ***
    private void zapiszGre() {
        try {
            FileOutputStream fileOut = new FileOutputStream("save.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(gd);
            out.close();
            fileOut.close();
        } catch (IOException i) {        	
            i.printStackTrace();
            out.println(i.getMessage());
        } catch (Exception ex) {        	
        	ex.printStackTrace();
        	out.println("Blad podczas zapisu gry");
        }
    }
    
    private GameData wczytajGre() {
        GameData data = null;
        try {
            FileInputStream fileIn = new FileInputStream("save.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            data = (GameData) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            pokazWiadomosc("Blad", "Brak zapisanych gier");
            
        } catch (ClassNotFoundException c) {                    
        	pokazWiadomosc("Blad", "Blad podczas wczytywania gry");
            
        } catch (Exception ex) {
        	pokazWiadomosc("Blad", "Blad podczas wczytywania gry");
        }

        return data;
    }
	

	// Wewnetrzne klasy

	private class Myszka extends MouseAdapter implements Serializable {

		private static final long serialVersionUID = 1L;

		@Override
		public void mousePressed(MouseEvent me) {
			
			mx = me.getX();
			my = me.getY();

			// System.out.println(String.format("(MX, MY)=(%d, %d)", mx, my));

			// Obsluga gry
			if (trwaGra) {
				// *** Obsluga GRY ***
				// Gdy nie ma jeszcze konca gry
				if (!gd.isKoniecGry()) {
					// *** odp A ***
					if (aBTN.getWidoczny()) {
						if (aBTN.jestKolizja(mx, my)) {
							sprawdzPytanie(1);
						}
					}

					// *** odp B ***
					if (bBTN.getWidoczny()) {
						if (bBTN.jestKolizja(mx, my)) {
							sprawdzPytanie(2);
						}
					}

					// *** odp C ***
					if (cBTN.getWidoczny()) {
						if (cBTN.jestKolizja(mx, my)) {
							sprawdzPytanie(3);
						}
					}

					// *** odp D ***
					if (dBTN.getWidoczny()) {
						if (dBTN.jestKolizja(mx, my)) {
							sprawdzPytanie(4);
						}
					}

					// *** Guzik POMOC podczas gry ***
					if (pomocBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'POMOC' button during game");
						if (gd.getIloscPodpowiedzi() > 0) {
							uzyjKola();
						} else {
							getMessageBox().setTitle("Brak podpowiedzi");
							JOptionPane.showMessageDialog(getMessageBox(), "Niestety nie masz juz wiecej podpowiedzi :(");
						}
					}
				}

				// *** Guzik WYJSCIE podczas gry ***
				if (wyjscieBTN.jestKolizja(mx, my)) {
					out.println("Clicked 'WYJSCIE' button during game");
					
					
					if(!gd.isKoniecGry()) {
						// Jesli gra sie jeszcze nie skonczyla to popros uzytkownika o potwierdzenie przerwania gry
						if(przerwijGre()) {
							trwaGra = false;
							zapiszGre();
						}							
					} else {
						// Jesli gra juz sie skonczyla (wyswietlony jest tylko koncowy rezultat) to wyjdz do menu glownego
						trwaGra = false;
						File f = new File("save.ser");
				        if (f.exists()) {
				            f.delete();
				        }
					}

				}
				
			} else {
				// *** Obsluga menu glownego ***
				
				if(pokazRanking) {
					// *** Guzik WYJSCIE w rankingu ***
					if (wyjscieBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'WYJSCIE' button in ranking");
						
						pokazRanking = false;
					}
					
				} else {
					// *** Guzik NOWA_GRA ***
					if (nowaGraBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'NOWA_GRA' button in main menu");
						
						if (pytaniaZaladowanePoprawnie) {
							if(!istniejaZapisaneGry()) {
								if(zapytajONick()) {
									File f = new File("save.ser");
							        if (f.exists()) {
							            f.delete();
							        }
							        
									rozpocznijGre();    
									
									pokazWiadomosc("10 pytan", zbudujWiadomoscPowitalna());
								}	
							}
							
							
						} else {
							pokazWiadomosc("Blad", "Brak pytan w bazie danych! Popraw plik z baza pytan!");
						}
						
					}

					// *** Guzik WCZYTAJ_GRE ***
					if (wczytajBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'WCZYTAJ_GRE' button in main menu");

						gd = wczytajGre();
						if(gd != null) {
							trwaGra = true;
						} else {
							gd = new GameData();
						}
					}
					
					// *** Guzik RANKING ***
					if (rankingBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'Ranking' button in main menu");
						
						rankingTop10 = ranking.SearchTop10();
						pokazRanking = true;
					}
					
					// *** Guzik OPCJE ***
					if (opcjeBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'OPCJE' button in main menu");
						pokazOpcje();
					}
					
					// *** Guzik POMOC ***
					if (pomocBTN.jestKolizja(mx, my)) {
						pokazWiadomosc("Pomoc", zbudujInstrukcjeGry());
					}

					// *** Guzik WYJSCIE ***
					if (wyjscieBTN.jestKolizja(mx, my)) {
						out.println("Clicked 'WYJSCIE' button in main menu");
						JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(getPanel());
						frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
						frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
					}
				}
				
			}

		}

		@Override
		public void mouseMoved(MouseEvent me) {
			motionX = me.getX();
			motionY = me.getY();

			// System.out.println(String.format("Motion=(%d, %d)", motionX,
			// motionY));
		}

	}

	// Animatory
	private class AnimatorGuzikow implements Runnable {

		public int opoznienieAnimacji;

		public AnimatorGuzikow(int opoznienie) {
			this.opoznienieAnimacji = opoznienie;
		}

		@Override
		public void run() {

			long beforeTime, timeDiff, sleep;

			beforeTime = System.currentTimeMillis();

			while (true) {

				// Zmien obraz guzikow
				if (trwaGra) {
					if(!gd.isKoniecGry()) {
						// *** odpowiedz A ***
						if (aBTN.jestKolizja(motionX, motionY)) {
							aBTN.setAktualnyObraz(aBTN.getObrazNajechany());
						} else {
							aBTN.setAktualnyObraz(aBTN.getObraz());
						}
						// *** odpowiedz B ***
						if (bBTN.jestKolizja(motionX, motionY)) {
							bBTN.setAktualnyObraz(bBTN.getObrazNajechany());
						} else {
							bBTN.setAktualnyObraz(bBTN.getObraz());
						}
						// *** odpowiedz C ***
						if (cBTN.jestKolizja(motionX, motionY)) {
							cBTN.setAktualnyObraz(cBTN.getObrazNajechany());
						} else {
							cBTN.setAktualnyObraz(cBTN.getObraz());
						}
						// *** odpowiedz D ***
						if (dBTN.jestKolizja(motionX, motionY)) {
							dBTN.setAktualnyObraz(dBTN.getObrazNajechany());
						} else {
							dBTN.setAktualnyObraz(dBTN.getObraz());
						}
					}					

				} else {
					
					if(!pokazRanking) {
						// *** NOWA GRA ***
						if (nowaGraBTN.jestKolizja(motionX, motionY)) {
							nowaGraBTN.setAktualnyObraz(nowaGraBTN.getObrazNajechany());
						} else {
							nowaGraBTN.setAktualnyObraz(nowaGraBTN.getObraz());
						}
						// *** WCZYTAJ GRE ***
						if (wczytajBTN.jestKolizja(motionX, motionY)) {
							wczytajBTN.setAktualnyObraz(wczytajBTN.getObrazNajechany());
						} else {
							wczytajBTN.setAktualnyObraz(wczytajBTN.getObraz());
						}
						// *** RANKING ***
						if (rankingBTN.jestKolizja(motionX, motionY)) {
							rankingBTN.setAktualnyObraz(rankingBTN.getObrazNajechany());
						} else {
							rankingBTN.setAktualnyObraz(rankingBTN.getObraz());
						}
						// *** OPCJE ***
						if (opcjeBTN.jestKolizja(motionX, motionY)) {
							opcjeBTN.setAktualnyObraz(opcjeBTN.getObrazNajechany());
						} else {
							opcjeBTN.setAktualnyObraz(opcjeBTN.getObraz());
						}
					}

				}

				// GUZIKI NIEZALEZNE OD STANU GRY
				// *** POMOC ***
				if (pomocBTN.jestKolizja(motionX, motionY)) {
					pomocBTN.setAktualnyObraz(pomocBTN.getObrazNajechany());
				} else {
					pomocBTN.setAktualnyObraz(pomocBTN.getObraz());
				}
				// *** WYJSCIE ***
				if (wyjscieBTN.jestKolizja(motionX, motionY)) {
					wyjscieBTN.setAktualnyObraz(wyjscieBTN.getObrazNajechany());
				} else {
					wyjscieBTN.setAktualnyObraz(wyjscieBTN.getObraz());
				}

				// ODSWIEZANIE GRAFIKI
				odswiezGrafike();

				timeDiff = System.currentTimeMillis() - beforeTime;
				sleep = opoznienieAnimacji - timeDiff;

				if (sleep < 0)
					sleep = 2;
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					System.out.println("interrupted");
				}

				beforeTime = System.currentTimeMillis();
			}
		}
	}

}
