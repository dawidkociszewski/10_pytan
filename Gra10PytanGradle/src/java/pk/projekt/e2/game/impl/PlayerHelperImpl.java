package pk.projekt.e2.game.impl;

import java.util.ArrayList;
import java.util.Random;

import pk.projekt.e2.game.contract.IPlayerHelper;

public class PlayerHelperImpl implements IPlayerHelper {

	/*
	 * (non-Javadoc)
	 * @see pk.projekt.e2.game.contract.IPlayerHelper#fiftyFifty(int)
	 * Metoda losuje 2 bledne odpowiedzi do wywalenia
	 */
	@Override
	public Integer[] fiftyFifty(int goodAns) {		
		Random r = new Random();
		
		// List wszystkich odpowiedzi
		ArrayList<Integer> wszystkieOdp = new ArrayList<Integer>();
		wszystkieOdp.add(1);
		wszystkieOdp.add(2);
		wszystkieOdp.add(3);
		wszystkieOdp.add(4);
		
		// Wywalamy wlasciwa odpowiedz
		wszystkieOdp.remove(Integer.valueOf(goodAns));
		
		// Losujemy jedna z zlych odpowiedzi do wywalenia
		int temp = r.nextInt(wszystkieOdp.size());
		
		// Wywalamy jedna z pozostalych zlych odpowiedzi
		wszystkieOdp.remove(Integer.valueOf(temp));
		
		Integer[] zleOdpDoWywalenia = wszystkieOdp.toArray(new Integer[wszystkieOdp.size()]);
		
		return zleOdpDoWywalenia;
	}

}
