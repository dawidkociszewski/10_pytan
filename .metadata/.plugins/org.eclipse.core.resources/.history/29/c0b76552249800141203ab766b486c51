package pk.projekt.e2.game.impl;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pk.projekt.e2.game.contract.IQuestionsDBManager;
import pk.projekt.e2.game.models.Question;

public class QuestionsDBManagerImpl implements IQuestionsDBManager, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String log;
	private ArrayList<Question> listaPytan;

	public QuestionsDBManagerImpl() {

		log = "[new->QuestionsDBManagerImpl]Stworzono nowa instacje QuestionsDBManagerImpl";
		listaPytan = new ArrayList<Question>();
	}


	@Override
	public boolean zaladujDane(String fileName) {
		try {
			File inFile = new File(fileName);

			if (inFile.exists()) {

				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(inFile);

				// read this -
				// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
				doc.getDocumentElement().normalize();

				System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("question");
				System.out.println(String.format("Liczba pytan w pliku: %d", nList.getLength()));

				System.out.println("----------------------------");
				
				int iloscDodanych = 0;

				for (int i = 0; i < nList.getLength(); i++) {

					Node nNode = nList.item(i);

					// Jesli nie udalo sie dodac element (mozliwe, ze blad w pliku XML) to skocz do nastepnego elementu
					if(!dodajPytanie(nNode)) {
						continue;
					} else {
						iloscDodanych++;
					}
				}
				
				if(iloscDodanych > 0) {
					log = String.format("[zaladujDane]Przetwarzanie pliku [%s] zakonczone. Ilosc blednych wpisow: %d/%d.", fileName, nList.getLength() - iloscDodanych, nList.getLength());
				} else {
					log = String.format("[zaladujDane]Blad podczas przetwarzania pliku [%s]. Wszystkie wpisy sa bledne!", fileName);
				}
				
				return true;
			}

		} catch (Exception e) {
			log = String.format("[zaladujDane]Blad podczas przetwarzania pliku [%s]", fileName);
			return false;
		}

		log = String.format("[zaladujDane]Blad! Plik %s nie istnieje!", fileName);
		return false;
	}
	
	private boolean dodajPytanie(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {

			Element ele = (Element) node;

			// Wczytanie numeru dobrej odpowiedzi
			String temp = ele.getAttribute("goodans").trim();
			int numerDobrejOdp = -1;
			try {
				numerDobrejOdp = Integer.parseInt(temp);
			} catch(NumberFormatException nfe) {
				//System.out.println(String.format("[EXCEPTION]numerDobrejOdp=%d", numerDobrejOdp));
				return false;
			}
			// Jesli numer dobrej odpowiedzi nie jest rowne <1, 4> to przerwij operacje
			if(numerDobrejOdp < 1 || numerDobrejOdp > 4) {
				//System.out.println(String.format("[BAD_ANS_VALUE]numerDobrejOdp=%d", numerDobrejOdp));
				return false;
			}
			//System.out.println(String.format("[AFTER_VALIDATION]numerDobrejOdp=%d", numerDobrejOdp));
			
			// Wczytanie tresci pytania
			String pytanie = ele.getElementsByTagName("q").item(0).getTextContent().trim();
			// Jesli pytanie jest puste lub null (blad w pliku XML) to przerwij operacje
			if(pytanie.isEmpty() || pytanie == null) {
				return false;
			}
						
			String odp1 = ele.getElementsByTagName("ans1").item(0).getTextContent().trim();
			// Jesli odp1 jest puste lub null (blad w pliku XML) to przerwij operacje
			if(odp1.isEmpty() || odp1 == null) {
				return false;
			}
			
			String odp2 = ele.getElementsByTagName("ans2").item(0).getTextContent().trim();
			// Jesli odp2 jest puste lub null (blad w pliku XML) to przerwij operacje
			if(odp2.isEmpty() || odp2 == null) {
				return false;
			}
			
			String odp3 = ele.getElementsByTagName("ans3").item(0).getTextContent().trim();
			// Jesli odp3 jest puste lub null (blad w pliku XML) to przerwij operacje
			if(odp3.isEmpty() || odp3 == null) {
				return false;
			}
			
			String odp4 = ele.getElementsByTagName("ans4").item(0).getTextContent().trim();
			// Jesli odp4 jest puste lub null (blad w pliku XML) to przerwij operacje
			if(odp4.isEmpty() || odp4 == null) {
				return false;
			}
			
			// Jezeli kod tu doszedl to znaczy, ze dane wejsciowe powinny byc w porzadku. Zatem dodaj nowe pytanie do listy
			listaPytan.add(new Question(pytanie, odp1, odp2, odp3, odp4, numerDobrejOdp));		
			return true;
		}		
		
		return false;
	}

	@Override
	public String pobierzLog() {		
		return this.log;
	}

	@Override
	public Question losujPytanie(ArrayList<Integer> uzytePytania) {
		
		// Warunek, aby baza pytan nie byla pusta
		if (listaPytan.size() > 0) {
			Random r = new Random();
			int nowe = -1;
			
			// Lista numerow mozliwych pytan 
			ArrayList<Integer> mozliwePytania = new ArrayList<Integer>();
			
			// Inicjalizacja listy
			for (int i = 0; i < listaPytan.size(); i++) {
				mozliwePytania.add(i);
			}
			
			// Usuniecie z mozliwych pytan liste uzytych pytan
			mozliwePytania.removeAll(uzytePytania);
			
			// Jesli wszystkie pytania zostaly juz zluzyte to nie uwzgledniaj liste uzytych pytan
			if (mozliwePytania.isEmpty()) {				

				nowe = r.nextInt(listaPytan.size());				
				
			} else {				
				// Jesli lista numerow mozliwych pytan nie jest pusta to wylosuj nowe pytanie
				nowe = mozliwePytania.get(r.nextInt(mozliwePytania.size()));								
			}
			
			log = String.format("[losujPytanie]Wylosowano pytanie o tresci: %s", listaPytan.get(nowe).getQuestion());
			return listaPytan.get(nowe);
		}

		log = "[losujPytanie]Baza danych pytan jest pusta. Zaladuj pytania!";
		return null;
	}
	
	// For debug purpose
	public void displayAllData() {
		for(Question q : listaPytan) {
			System.out.println("Lista pytan w bazie danych:");
			System.out.println("------------------------------------------------------------");
			System.out.println(String.format("[%d] %s", q.getGoodAns(), q.getQuestion()));
			System.out.println(String.format("(1) %s", q.getAns1()));
			System.out.println(String.format("(2) %s", q.getAns2()));
			System.out.println(String.format("(3) %s", q.getAns3()));
			System.out.println(String.format("(4) %s", q.getAns4()));			
		}
		System.out.println("------------------------------------------------------------");
	}


	@Override
	public int iloscPytanWBazie() {
		if(listaPytan == null) {
			return 0;
		}		
		
		return this.listaPytan.size();
	}
}
