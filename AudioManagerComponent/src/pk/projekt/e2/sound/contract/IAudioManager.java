package pk.projekt.e2.sound.contract;

import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public interface IAudioManager {

	public boolean addSound(String soundName, String path);
	public ArrayList<String> listAllFiles();
	public void playSound(String soundName) throws LineUnavailableException, IOException, UnsupportedAudioFileException, InterruptedException;
	public String getLatestLog();
	
}
