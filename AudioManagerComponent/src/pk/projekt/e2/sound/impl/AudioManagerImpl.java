package pk.projekt.e2.sound.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import pk.projekt.e2.sound.contract.IAudioManager;

public class AudioManagerImpl implements IAudioManager {

	/*
	 * Private Variables
	 */
	private Clip clip;
	private HashMap<String, String> soundList = new HashMap<String, String>();
	/*
	 * Log is used to get latest operation log
	 */
	private String log;

	public AudioManagerImpl() {
	}

	/*
	 * name = name of a file in AudioManager list 
	 * path = full absolute path to
	 */
	@Override
	public boolean addSound(String name, String path) {
		// Checking if provided name exists in sound list
		if (soundList.containsKey(name)) {
			log = String.format("Name [%s] is already used", name);
			return false;
		}

		File soundFile = new File(path);
		if (soundFile.exists()) {
			if (soundList.containsValue(path)) {
				log = String.format("File [%s] already exist in sound list", path);
				return false;
			} else {
				String s = soundFile.getName();
				if (s.endsWith(".au") || s.endsWith(".rmf") || s.endsWith(".mid") || s.endsWith(".wav") || s.endsWith(".aif") || s.endsWith(".aiff")) {
					soundList.put(name, path);
					log = String.format("Added sound(%s, path=%s) successfully", name, path);
					return true;
				} else {
					log = String.format("Unsupported sound format (%s)", s);
					return false;
				}
			}
		}

		log = String.format("File [%s] does not exist!", path);
		return false;
	}

	@Override
	public ArrayList<String> listAllFiles() {
		ArrayList<String> files = new ArrayList<String>();

		int index = 0;

		for (String k : soundList.keySet()) {
			index++;
			files.add(String.format("(%d) Name=%s, Path=%s", index, k, soundList.get(k)));
		}

		return files;
	}

	@Override
	public void playSound(String soundName) throws LineUnavailableException, IOException, UnsupportedAudioFileException, InterruptedException {
		if (!soundList.containsKey(soundName)) {
			log = String.format("Sound (%s) does not exist!", soundName);
		} else {
			AudioListener alistener = new AudioListener();
			File soundFile = new File(soundList.get(soundName));
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
			try {
				clip = AudioSystem.getClip();
				clip.addLineListener(alistener);
				clip.open(audioInputStream);
				try {
					System.out.println("Playing: " + soundFile.getName());
					clip.start();
					alistener.waitUntilDone();
				} finally {
					clip.close();
				}
			} finally {
				audioInputStream.close();
				log = String.format("Playing %s sound completed", soundName);
			}
		}
	}

	/*
	 * Private audio listener
	 */
	private class AudioListener implements LineListener {
		private boolean done = false;

		@Override
		public synchronized void update(LineEvent event) {
			LineEvent.Type eventType = event.getType();

			if (eventType == LineEvent.Type.OPEN) {
				System.out.println("OPEN");
			} else if (eventType == LineEvent.Type.START) {
				System.out.println("START");
			} else if (eventType == LineEvent.Type.STOP || eventType == LineEvent.Type.CLOSE) {
				if (eventType == LineEvent.Type.STOP) {
					System.out.println("STOP");
					clip.stop();
				} else if (eventType == LineEvent.Type.CLOSE) {
					System.out.println("CLOSE");
					clip.close();
				}
				done = true;
				notifyAll();
			}
		}

		public synchronized void waitUntilDone() throws InterruptedException {
			while (!done) {
				wait();
			}
		}
	}

	@Override
	public String getLatestLog() {
		return this.log;
	}
}
